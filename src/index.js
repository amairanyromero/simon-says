const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');

//Initiliazations
 const app = express();


//Settings
app.set('port', process.env.PORT || 3000);


//Middlewares

//Routes


//Static files


//Server is listening
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});